# Joe Fear Netwrix Application

The instructions below should allow you to run the ASP.NET MVC application that you asked me to 
construct for my technical test. If it does not build using the material that I have sent and
by following these instructions then please let me know. This was designed using Visual
Studio Community Edition 2017 and SQL Server Express 2018, if you have these versions available
to you then I would recommend to use these. 

INSTRUCTIONS

A) Create a database in SQL Server - I would suggest 'JoeFearNetwrixChallenge' as this will mean less
will need to be changed for the connectionString. 

B) In the directory titled 'SQL Scripts' you will find two scripts. Firstly, run 
'CreatingAndSeedingTables.sql' which will create the tables and insert a small of data into them
to allow you to see the application running with some example data. After this, then
run 'CreateStoredProcedures.sql'.

C) Check the 'packages' directory for the following NuGet packages:
	- Antlr
	- jquery
	- jquery.datatables
	- bootstrap
	- jquery.unobtrusive.ajax
	- AspNet.Web.Optimization
	- AspNet.Mvc
	- AspNet.Razor
	- Webgrease
	- AspNet.WebPages
	- Web.Infrastructure
	- CodeDom.Providers.DotNetCompilerPlatform

If these are not included then you will need to install the latest stable version from NuGet package 
manager.

D) Open 'JoeFearNetwrixChallengeSolution.sln'

E) Navigate to the web.config file and observe the connection string:

  <connectionStrings>
    <add name="defaultConnectionString" connectionString="data source=JOE\SQLEXPRESS;
    initial catalog=JoeFearNetwrixChallenge;integrated security=True;"
    providerName="System.Data.SqlClient" />
  </connectionStrings>  

You will need to change the above connection string to relate to your SQL server. If your 
database is called 'JoeFearNetwrixChallenge' then all you should need to change is the
value of 'data source' as your instance will be different to 'JOE\SQLEXPRESS'

F) Launch the solution in google chrome. 

Like I said, let me know if you have any questions!
